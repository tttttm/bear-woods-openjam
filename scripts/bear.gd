extends KinematicBody2D

var is_climbing = false
var is_jumping = false
var is_pumping = false
var pump_toggle = 0

var GRAVITY = 5000
var SPEED = 1000
var JUMP_SPEED = 2000

var velocity = Vector2(0, 0)
var direction = 1
var freeze = false
var hooked = false

func get_input():
	velocity.x = 0
	
	var action_left = Input.is_action_pressed("ui_left")
	var action_right = Input.is_action_pressed("ui_right")
	var action_jump = Input.is_action_just_pressed("ui_up")
	var action_down = Input.is_action_just_pressed("ui_down")
	
	var action_shoot = Input.is_action_just_pressed("jump")

	if freeze:
		$lower_body.play("idle")
		if action_shoot and hooked:
			if pump_toggle == 0:
				$upper_body.play("pump0")
				pump_toggle = 1
			else:
				$upper_body.play("pump1")
				pump_toggle = 0
	else:
		if action_shoot and is_on_floor() and not is_pumping:
			$pump.shoot()
			$upper_body.play("pump1")
		else:
			$upper_body.play("pump0")
		
		# Jump
		if action_jump and is_on_floor():
			# Drop through platforms
			if action_down:
				position.y += 1
			# Jump normally
			else:
				is_jumping = true
				velocity.y -= JUMP_SPEED
	
		# Move
		if action_left:
			direction = -1
			velocity.x -= SPEED
			$upper_body.set_flip_h(true)
			$lower_body.set_flip_h(true)
			$lower_body.play("walk")

			$pump.position.x = -abs($pump.position.x)
			$pump.rotation_degrees = 180
			$pump.direction = -1
		
		elif action_right:
			direction = 1
			velocity.x += SPEED
			$upper_body.set_flip_h(false)
			$lower_body.set_flip_h(false)
			$lower_body.play("walk")
		
			$pump.position.x = abs($pump.position.x)
			$pump.rotation_degrees = 0
			$pump.direction = 1
			
		else:
			$lower_body.play("idle")
	
		# Animate jumping/falling
		if not is_on_floor():
			$lower_body.play("jump")

func _process(_delta):
	hooked = $pump/tip.is_hooked
	freeze = $pump.is_flying or hooked

func _physics_process(delta):
	get_input()
	velocity.y += GRAVITY * delta
	
	if is_jumping and is_on_floor():
		is_jumping = false
	velocity = move_and_slide(velocity, Vector2.UP, true)
