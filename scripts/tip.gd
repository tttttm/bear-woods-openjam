extends Area2D

var is_hooked = false
var hooked_node = null
var pump_toggle = 0

func _ready():
	#warning-ignore:return_value_discarded
	connect("body_entered", self, "_on_tip_body_entered")

func _input(_event):
	var action_shoot = Input.is_action_just_pressed("jump")
	
	if is_hooked:
		hooked_node.is_hooked = true
		if action_shoot:
			if pump_toggle == 0:
				pump_toggle = 1
			else:
				hooked_node.inflation_counter += 1
				pump_toggle = 0

func _process(_delta):
	if not hooked_node == null and hooked_node.is_balloon:
		is_hooked = false
		hooked_node = null

func _on_tip_body_entered(body):
	if "camper" in body.name and not is_hooked:
		hooked_node = body
		hooked_node.is_hooked = true
		is_hooked = true
