extends Node2D

var spawn_position = self.position
var CAMPER = preload ("res://scenes/camper.tscn")
var num_campers = 0
var spawn_ready = true

# Called when the node enters the scene tree for the first time.
func _ready():
	$spawn_delay.connect("timeout", self, "_on_spawn_delay_timeout")

func _process(_delta):
	if GLOBAL.num_campers < GLOBAL.MAX_CAMPERS and spawn_ready and GLOBAL.ready:
		var camper = CAMPER.instance()
		camper.position = self.position
		camper.direction = -1
		add_child(camper)
		GLOBAL.num_campers += 1
		$spawn_delay.start(5)

func _on_spawn_delay_timeout():
	spawn_ready = true

