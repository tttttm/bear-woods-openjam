extends KinematicBody2D

var rng = RandomNumberGenerator.new()

const GRAVITY = 50
var SPEED = 500
const FLOOR = Vector2.UP
const BALLOON_SPEED = 5
const DESPAWN_CEILING = -2000
const SPEED_MOD = 400

var is_dead = false
var is_hooked = false
var is_balloon = false
export var direction = 1
var velocity = Vector2()

var pump_toggle = 0
var inflation_counter = 0

func _ready():
	rng.randomize()
	SPEED += int(rng.randf_range(SPEED_MOD*-1, SPEED_MOD))

func balloon():
	is_hooked = false
	is_balloon = true
	$CollisionShape2D.set_disabled(true)
	$Area2D/CollisionShape2D.set_disabled(true)
	GLOBAL.num_campers -= 1
	GLOBAL.campers_removed += 100

func _process(_delta):
	if is_balloon and global_position.y <= DESPAWN_CEILING:
		self.queue_free()

func _physics_process(_delta):
	if is_hooked:
		velocity = Vector2()
		if inflation_counter >= 3:
			balloon()
		else:
			$AnimatedSprite.play("inflate"+str(inflation_counter))
	elif is_balloon:
		position.y -= BALLOON_SPEED
	else:
		# Move
		velocity.x = SPEED * direction
		velocity.y += GRAVITY
		velocity = move_and_slide(velocity, FLOOR)
	
		# Animate
		if direction == 1:
			$AnimatedSprite.flip_h = false
			$AnimatedSprite.play("walk")
		elif direction == -1:
			$AnimatedSprite.flip_h = true
			$AnimatedSprite.play("walk")
		else:
			$AnimatedSprite.play("idle")
		
		# Detect platform edges
		if is_on_wall():
			direction *= -1
			$RayCast2D.position.x *= -1
			$RayCast2D.rotation *= -1
