extends Node2D

var direction = 1
var tip_origin

const SPEED = 20
const MAX_HOSE_LEN = 750

var is_flying = false
var is_hooked = false

func _init():
	self.visible = false

func _ready():
	tip_origin = $tip.position

func shoot():
	$tip/CollisionShape2D.set_disabled(false)
	tip_origin = $tip.position
	self.visible = true
	is_flying = true

func release():
	$tip/CollisionShape2D.set_disabled(true)
	$tip.position = tip_origin
	self.visible = false
	is_flying = false
	
func _process(_delta):
	
	if $tip.is_hooked:
		is_flying = false
	elif is_flying:
		$hose.region_rect.size.x = abs($tip.position.x - tip_origin.x)
		$hose.position.x = $hose.region_rect.size.x/2
		
		if $hose.region_rect.size.x > MAX_HOSE_LEN:
			release()
	else:
		release()
		
func _physics_process(_delta):
	if is_flying:
		if direction == 1:
			$tip.position.x += direction * SPEED
		else:
			$tip.position.x -= direction * SPEED
