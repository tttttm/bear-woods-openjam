# Bear Woods

"Local bear finds bike pump"

[PLAY IT HERE](https://timpark.itch.io/bear-woods)

## About

Bear woods is a submission for [Open Jam](https://itch.io/jam/open-jam-2020), a weekend-long game jam that promotes the use of open source tools and platforms. This year's theme was `Airborne`.

## Concept

For some reason, my mind went almost straight to Dig Dug. My line of thinking was highly sophisticated: "airborne" to "air" to "bike pump" to "Dig Dug". I thought it would be funny to put a spin on that classic inflation mechanic I enjoyed so much as a kid.

Originally the enemies were supposed to be [cuboid dogs](https://gitlab.com/timparktimpark/bear-woods/-/blob/master/assets/dog/idle.png). But after I drew them and dropped them into the game, they ended up being too short for the air pump's trajectory. Rather than saving precious time by modifying my existing drawings, I made the irrational decision to draw a new enemy: the common boy scout. This change was ultimately for the better, both mechanically and thematically: the air pump lined up perfectly with the child's head, and the scouts made more sense in the woodland setting of the game.

Due to time constraints, the "finished" game only features the female version of the scout. The original boy scout assets are buried in a Krita file somewhere in this repository.

![concept_sketch](mockups/initial_sketch.jpg)